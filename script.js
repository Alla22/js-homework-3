// Практичні завдання.
// 1. Попросіть користувача ввести свій вік за допомогою prompt.
// Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те,
// що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те,
// що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.

// Завдання з підвищенною складністю.
// Перевірте чи ввів користувач саме число в поле prompt. Якщо ні, то виведіть повідомлення, що введено не число.
// 2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення,
// скільки днів у цьому місяці. Результат виводиться в консоль.
// Скільки днів в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81)
// (Використайте switch case)

"use strict"

let age = prompt("Please, enter your age:" , "0");



if (age < 12){
alert(`Your age is ${age}, you are a child.`);
}
else if(age < 18) {
    alert(`Your age is ${age}, you are a teenager.`);
}
else{
    alert(`Your age is ${age}, you are an adult.`);
}


// 2.task

let month = prompt("Введіть місяць року, щоб побачити кількість днів:");
switch(month){
    case"січень":
    console.log("31");
    break;

    case"лютий":
    console.log("29");
    break;

    case"березень":
    console.log("31");
    break;

    case"квітень":
    console.log("30");
    break;


    case"травень":
    console.log("31");
    break;



    case"червень":
    console.log("30");
    break;


    case"липень":
    console.log("31");
    break;

    case"серпень":
    console.log("31");
    break;

    case"вересень":
    console.log("30");
    break;

    case"жовтень":
    console.log("31");
    break;

    case"листопад":
    console.log("30");
    break;

    case"грудень":
    console.log("31");
    break;

    default:
        console.log("Оберіть місяць.");
        break;
}
